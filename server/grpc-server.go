package server

import (
	"context"
	"gitlab.com/citaces/grpc-example/proto"
)

// GRPCServer ...

type GRPCServer struct {
	proto.UnimplementedAdderServer
}

// Add ...

func (g *GRPCServer) Add(c context.Context, req *proto.AddRequest) (*proto.AddResponse, error) {
	return &proto.AddResponse{
			Result: req.GetX() + req.GetY(),
		},
		nil
}
