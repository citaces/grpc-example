package main

import (
	"gitlab.com/citaces/grpc-example/proto"
	"gitlab.com/citaces/grpc-example/server"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	s := grpc.NewServer()
	g := &server.GRPCServer{}
	proto.RegisterAdderServer(s, g)
	l, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal(err)
	}
	if err = s.Serve(l); err != nil {
		log.Fatal(err)
	}
}
