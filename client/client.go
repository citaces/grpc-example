package main

import (
	"context"
	"errors"
	"flag"
	"gitlab.com/citaces/grpc-example/proto"
	"google.golang.org/grpc"
	"log"
	"strconv"
)

func main() {

	flag.Parse()

	if flag.NArg() < 2 {
		log.Fatal(errors.New("low symbols"))
	}

	x, err := strconv.Atoi(flag.Arg(0))
	if err != nil {
		log.Fatal(err)
	}
	y, err := strconv.Atoi(flag.Arg(1))
	if err != nil {
		log.Fatal(err)
	}

	g, err := grpc.Dial(":8080", grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}

	cl := proto.NewAdderClient(g)

	res, err := cl.Add(context.Background(), &proto.AddRequest{X: int32(x), Y: int32(y)})
	if err != nil {
		log.Fatal(err)
	}
	log.Println(res.Result)

	//go run client.go 5 15

}
